<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Jaden
  Date: 2020-01-20
  Time: 6:45 p.m.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <title>PROG3060: JA Exercise 1</title>
  </head>
  <body>
    <h1>Name: Jaden Ji Hong Ahn</h1>
    <h2>Java Enterprise: Section 1</h2>
    <p><%=new Date()%></p>
  </body>
</html>
