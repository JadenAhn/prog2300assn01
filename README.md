**Running a Java EE Application﻿**

## Exploring a run configuration﻿
When creating the project, we specified GlassFish as an application server. As a result, IntelliJ IDEA created a run configuration for GlassFish.

When we performed the Run command icons toolwindows toolWindowRun, we started that run configuration. Now let's take a look at the run configuration and see how its settings map onto the events that we've just observed.

1. Click the run configuration selector and select Edit Configurations.
The Run/Debug Configurations dialog opens and the settings for the GlassFish run configuration are shown.
The Before launch task list (in the lower part of the dialog) specifies that the application code should be compiled and the corresponding artifact should be built prior to executing the run configuration.

2. Select the Startup/Connection tab to see how the server is started in the run, debug and code coverage modes.

3. Select the Deployment tab to see which artifacts are deployed after the server is started.

4. Go back to the Server tab.
The settings under Open browser specify that after launch (i.e. after the server is started and the artifacts are deployed onto it) the default web browser should start and go to the specified URL http://localhost:8080/JavaEEHelloWorld_war_exploded. The settings to the right of On 'Update' action specify that on clicking update icon in the Run tool window the Update dialog should be shown and the Update resources option should be used by default. (The last used update option becomes the default one).

5. Click OK.

## Why chose MIT License
The MIT License is short and to the point. It lets people do almost anything they want with your project, like making and distributing closed source versions. This project is just a tutorial to Java EE Application, so I chose MIT License. 

## License Information
MIT License

Copyright (c) 2020 Jaden Ahn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.